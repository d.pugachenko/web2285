function click1() {
  var Cena = document.getElementsByName("Cena");
  var Kolvo = document.getElementsByName("Kolvo");
  var result = document.getElementById("result");
  var pCena = Cena[0].value;
  var pKolvo = Kolvo[0].value;
  var f1 = pCena.match(/^\d+$/);
  var f2 = pKolvo.match(/^\d+$/);
  if (Cena[0].value === "" && Kolvo[0].value === "") {
    result.innerHTML = "Вы не указали цену и количество товара";
  } else if (Cena[0].value === "") {
    result.innerHTML = "Вы не указали цену товара";
  } else if (Kolvo[0].value === "") {
    result.innerHTML = "Вы не указал кол-во товара";
  } else {
    if (f1 !== null && f2 !== null) {
      result.innerHTML =
        "Стоимость покупки: " +
        parseInt(Cena[0].value) * parseInt(Kolvo[0].value) +
        " руб";
    } else if (f1 === null && f2 !== null) {
      result.innerHTML = "Непрвильно введены данные в первом поле";
    } else if (f1 !== null && f2 === null) {
      result.innerHTML = "Непрвильно введены данные во втором поле";
    } else if (f1 === null && f2 === null) {
      result.innerHTML = "Непрвильно введены данные в обоих полях";
    }
  }
}

function onClick() {
  alert("Работа завершена");
}

window.addEventListener("DOMContentLoaded", function (event) {
  console.log("DOM fully loaded and parsed");
  let b = document.getElementById("calc");
  b.addEventListener("click", onClick);
});
